package dizari.challenge.bank.Interface;

import org.springframework.data.repository.CrudRepository;

import dizari.challenge.bank.Model.Account;

public interface AccountRepository extends CrudRepository<Account, Integer>{

}
