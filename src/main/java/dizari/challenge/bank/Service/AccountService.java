package dizari.challenge.bank.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dizari.challenge.bank.Interface.AccountRepository;
import dizari.challenge.bank.Model.Account;

@Service
public class AccountService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	public List<Account> getAllAccounts(){
		List<Account> accounts = new ArrayList<>();
		accountRepository.findAll().forEach(accounts::add);
		return accounts;
	}
	
	public Optional<Account> getAccount(int id){
		
		return accountRepository.findById(id);
	}

	public void createAccount(Account account) {
		accountRepository.save(account);
	}
	
	public void updateAccount(Account account, int id) {	
		accountRepository.save(account);
	}
}
