package dizari.challenge.bank.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dizari.challenge.bank.Model.Account;
import dizari.challenge.bank.Service.AccountService;

@RestController
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	@RequestMapping(method=RequestMethod.POST, value="/api/create-account")
	public void createAccount(@RequestBody Account account) {
		accountService.createAccount(account);
	}
	
	@RequestMapping("/api/get-accounts")
	public List<Account> viewAllAccounts() {
		return accountService.getAllAccounts();
	}
	
	@RequestMapping("/api/get-account/{id}")
	public Optional<Account> viewAccount(@PathVariable int id) {
		return accountService.getAccount(id);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/api/update-account/{id}")
	public void updateAccount(@RequestBody Account account, @PathVariable int id) {
		accountService.updateAccount(account, id);
	}
}
