package dizari.challenge.bank;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppApi {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(AppApi.class);
        app.setDefaultProperties(Collections
          .singletonMap("server.port", "9999"));
        app.run(args);
	}
}
